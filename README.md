# harold

A Bluetooth fuzzer.

## Installation
### Standard
* `python3 setup.py install`
* `harald -h`

### Development
* It's recommended to use a virtualenv:
    * `virtualenv --python3 python venv`
    * `source venv/bin/activate`
* `python setup.py develop`
* `harald -h`