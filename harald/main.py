#Application imports.
import sys
import signal
import argparse
import os
import time
import logging
from loguru import logger
import random

import pygatt
from binascii import hexlify
import pyradamsa

import harald
import harald.utils as hutils

# Handles early quitters.
def signal_handler(signal, frame):
    print("")
    quit(0)

# Argument parsing which outputs a dictionary.
def parseArgs():
    #Setup the argparser and all args
    parser = argparse.ArgumentParser(prog = harald.PROG_NAME, description = harald.PROG_DESC, epilog = harald.PROG_EPILOG)
    parser.add_argument("-v", "--version", action="version", version="%(prog)s " + harald.VERSION)
    parser.add_argument("-q", "--quiet", help="surpress extra output", action="store_true", default=False)

    parser.add_argument("-b", "--bd-addr", help="Bluetooth LE device address to connect to", required = True)
    parser.add_argument("-w", "--write-char", help="GATT char to fuzz", required = True)
    parser.add_argument("-i", "--input", help="Input sample to use as a base for mutation [deadbeef]", default = "deadbeef", type = str)
    parser.add_argument("-s", "--seed", help="Input for RNG to use []", default = "", type = str)
    parser.add_argument("-a", "--adapter", help="Bluetooth LE adapter to use [hci0]", default = "hci0")
    parser.add_argument("-n", "--num-tests", help="Number of test cases to run, or '0' to run forever [100]", default = 100, type = int)
    parser.add_argument("-u", "--update", help="Update notification interval [10]", default = 10, type = int)
    parser.add_argument("-m", "--multi", help="Run the entire suite of tests, even if one appears to have succeeded [False]", default = False, action = "store_true")

    parser.add_argument("--single", help="Run a single input, usually as a reponse to a previous failed test", default = False, action = "store_true")

    parser.add_argument("-d", "--debug", help="Show debug messages [False]", default = False, action = "store_true")
    
    return parser.parse_args()


# For a test, it writes to one char, and listens to another. It expects a response to each write. Returns whether or nto the device responded to the write in time.
def run_one_test(adapter, bd_addr, write_char, value):
    logger.debug("Running one test with input [%s]..." % value.hex()) 

    try:
        device = adapter.connect(bd_addr)
    except:
        logger.error("Failed to connect to the device with address '%s'." % bd_addr)
        quit(1)

    try:
        handle = device.get_handle(write_char)
    except:
        logger.error("The characteristic UUID '%s' appears to be incorrect, and could not be found." % write_char)
        quit(1)
    
    try:
        device.char_write_handle(handle, value, wait_for_response = True)
        device.disconnect()
        reponded_correctly = True
    except:
        logger.warning("The target device has failed to respond to the input [%s]" % value.hex())
        reponded_correctly = False
    finally:
        
        logger.debug("Single test completed.")

    return reponded_correctly

#Main application entry point.
def main():
    #Signal handler to catch CTRL-C (quit immediately)
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)

    argv = parseArgs()

    if not argv.seed == "":
        random.seed(argv.seed)    

    #Print out some sweet ASCII art.
    if not argv.quiet:
        hutils.print_header()

    logger.remove()
    if argv.debug:
        logger.add(sys.stderr, level = "DEBUG")
    else:
        logger.add(sys.stderr, level = "INFO")

    # Begin application logic.
    logger.info("Starting harald fuzzer...")

    try:
        logger.info("Starting adapter '%s'..." % argv.adapter)
        adapter = pygatt.GATTToolBackend(hci_device = argv.adapter)
    except:
        logger.error("Failed to init the `gatttool` backend. Make sure you have it installed in your system.")

    try:
        adapter.start()

        input_sample = argv.input

        if argv.single:
            reponded_correctly = run_one_test(adapter, argv.bd_addr, argv.write_char, bytes.fromhex(argv.input))

            if reponded_correctly:
                logger.info("The test responded to the write correctly. Any found value may be a false positive.")
            else:
                logger.info("The test did *not* respond to the write correctly. If the target is reliably unresponsive after this test, you may have a finding.")
            quit(0)

        if argv.num_tests == 0:
            test_num = 0
            while True:
                if test_num % argv.update == 0 and not test_num == 0:
                    logger.info("%d tests run." % test_num)
                
                value = pyradamsa.Radamsa().fuzz(bytes.fromhex(input_sample), seed = random.randint(0, sys.maxsize))
                reponded_correctly = run_one_test(adapter, argv.bd_addr, argv.write_char, value)
                test_num += 1

                if not reponded_correctly and not argv.multi:
                    logging.info("Single failed test case found, and multi-case collection not enabled. Exiting now.")
                    break

        else:
            for test_num in range(argv.num_tests):
                if test_num % argv.update == 0 and not test_num == 0:
                    logger.info("%d tests run."% test_num)

                value = pyradamsa.Radamsa().fuzz(bytes.fromhex(input_sample), seed = random.randint(0, sys.maxsize))
                reponded_correctly = run_one_test(adapter, argv.bd_addr, argv.write_char, value)

                if not reponded_correctly and not argv.multi:
                    logging.info("Single failed test case found, and multi-case collection not enabled. Exiting now.")
                    break
    finally:
        adapter.stop()
        logger.info("Exiting harald...")

if __name__ == "__main__":
    main()
    quit(0)