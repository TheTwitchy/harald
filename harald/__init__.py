#Application global vars
VERSION = "0.1"
PROG_NAME = "harald"
PROG_DESC = "A Bluetooth fuzzer."
PROG_EPILOG = "Written by TheTwitchy. Source available at https://gitlab.com/TheTwitchy/harald"
DEBUG = False