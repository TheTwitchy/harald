import setuptools
from harald import VERSION

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="harald",
    author="TheTwitchy",
    version=VERSION,
    author_email="thetwitchy@thetwitchy.com",
    description="A Bluetooth fuzzer.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/TheTwitchy/harald",
    license="MIT",
    packages=setuptools.find_packages(),
    install_requires=[
        "loguru",
        #"pyradamsa",
        "pygatt",
        "pygatt[GATTTOOL]"
    ],
    entry_points = {
        'console_scripts': [
            'harald = harald.main:main',                  
        ],              
    },
)
